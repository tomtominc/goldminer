﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using PixelArtRotation;
using UnityEditor.Animations;

[RequireComponent(typeof(BoxCollider2D))]
public class Hook : MonoBehaviour
{
    public enum State 
    {
        Rotate,
        Shoot,
        Reel
    }


    public float angleOffset = 45f;
    public float shootDistance = 10f;
    public float rotationSpeed = 16f;
    public int minAngle = -45;
    public int maxAngle = 45;
    public float shootDuration = 1f;
    public float reelDuration = 1f;

    public SpriteRenderer chain;
    protected State currentState;
    protected Vector2 originalPosition;
    protected int currentAngle = 0;
    protected int currentDirection = 1;

    protected PixelRotation pixelRotation;

    protected Tweener shootTween;
    protected Tweener reelTween;

    private void Awake()
    {
        originalPosition = transform.position;
        pixelRotation = GetComponentInChildren < PixelRotation >();
    }

    private void OnEnable()
    {
        Rotate();
    }

    private void OnDisable()
    {
        
    }

    private void DisableAllTweens()
    {
        if (shootTween != null && shootTween.IsActive())
            shootTween.Kill();

        if (reelTween != null && reelTween.IsActive())
            reelTween.Kill();
    }

    private void Update()
    {
        switch (currentState)
        {
            case State.Rotate:
                Rotate();
                break;
        }

        // update the chain
        chain.size = new Vector2(chain.size.x, Vector2.Distance(originalPosition, transform.position));
    }

    private void OnTriggerEnter2D(Collider2D other)
    {   
        Collectable collectable = other.gameObject.GetComponent < Collectable >();

        if (collectable)
        {
            Reel(collectable);
        }
    }

    protected virtual void Rotate()
    {
        currentAngle += (int)(rotationSpeed * currentDirection);

        if (currentAngle >= maxAngle)
        {
            currentAngle = maxAngle;
            currentDirection = -1;
        }

        if (currentAngle <= minAngle)
        {
            currentAngle = minAngle;
            currentDirection = 1;
        }

        chain.transform.eulerAngles = new Vector3(0f, 0f, currentAngle);
        transform.eulerAngles = new Vector3(0f, 0f, currentAngle);

        if (Input.GetMouseButtonDown(0))
            Shoot();
           
    }

    protected virtual void Shoot()
    {
        currentState = State.Shoot;
        float radians = Mathf.Deg2Rad * (float)(currentAngle + angleOffset);
        Vector2 direction = new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));
        Vector2 endPosition = transform.GetChild(0).position + ((Vector3)direction * shootDistance);

        shootTween = transform.DOMove(endPosition, 1f, false).OnComplete(() => Reel(null));

        Debug.DrawLine(transform.position, endPosition, Color.red, 10f);
    }

    protected virtual void Reel(Collectable collectable)
    {
        currentState = State.Reel;

        DisableAllTweens();

        if (collectable == null)
        {
            reelTween = transform.DOMove(originalPosition, reelDuration, false).OnComplete  
                (() => currentState = State.Rotate);
        }
        else
        {
            collectable.transform.SetParent(transform);

            reelTween = transform.DOMove(originalPosition, reelDuration + collectable.weight, false).OnComplete  
                (() => currentState = State.Rotate);
        }


    }

   
}
