﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{
    public GameManager gameManager;

    public virtual GameStateKey value
    {
        get { return GameStateKey.None; }
    }

    public void InitializeState(GameManager gameManager)
    {
        this.gameManager = gameManager;
    }

    public virtual void StartState()
    {
        
    }

    public virtual void UpdateState()
    {
        
    }

    public virtual void EndState()
    {
        
    }

    public virtual bool WantsControl()
    {
        return false;
    }

}
