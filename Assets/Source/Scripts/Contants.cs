﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    public static Vector2 bounds
    {
        get { return new Vector2(Camera.main.orthographicSize * 0.5f, Camera.main.orthographicSize); }
    }
}
