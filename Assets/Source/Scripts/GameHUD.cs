﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class GameHUD : MonoBehaviour
{
    public Text timeLabel;
    public Text goldLabel;

    public EventHandler timeExpired;

    //    public void OnNewGameStarted(GameSettings settings)
    //    {
    //        currentGold = 0;
    //        currentTime = settings.maxTime;
    //    }
    //
    //    public void AddGold(int goldAmount)
    //    {
    //        currentGold += goldAmount;
    //
    //        UpdateGoldHUD();
    //    }
    //
    //    public void RemoveGold(int goldAmount)
    //    {
    //        currentGold -= goldAmount;
    //
    //        UpdateGoldHUD();
    //    }
    //
    //    public void UpdateTime()
    //    {
    //        if (currentTime <= 0)
    //            return;
    //
    //        currentTime -= Time.deltaTime;
    //
    //        if (currentTime <= 0f)
    //        {
    //            if (timeExpired != null)
    //                timeExpired(this, null);
    //        }
    //
    //        UpdateTimeHUD();
    //    }
    //
    //    public void UpdateTimeHUD()
    //    {
    //        timeLabel.text = currentTime.ToString("'Time:' ##");
    //    }
    //
    //    public void UpdateGoldHUD()
    //    {
    //        goldLabel.text = currentGold.ToString("'Gold:' ##");
    //    }
}
