﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum GameStateKey
{
    None,
    LevelIntro,
    Gameplay,
    LevelWin,
    LevelLose,
    Shop
}

[System.Serializable]
public class GameSettings
{
    public float maxTime;
    public float goldTarget;
    public float timeLeft;
    public float currentGold;
}

public class GameManager : MonoBehaviour
{
    public GameStateKey startingGameState;
    public GameSettings overrideSettings;

    protected GameState currentState;
    protected Dictionary < GameStateKey ,  GameState > gameStates;

    public GameSettings gameSettings
    {
        get { return overrideSettings; }
    }

    public void Start()
    {
        gameStates = new Dictionary<GameStateKey, GameState>();
        var states = GetComponentsInChildren < GameState >().ToList();

        foreach (var state in states)
            gameStates.Add(state.value, state);

        ChangeGameState(startingGameState);
    }

    public void ChangeGameState(GameStateKey key)
    {
        if (currentState != null)
            currentState.EndState();

        if (gameStates.ContainsKey(key))
            currentState = gameStates[key];

        if (currentState != null)
            currentState.StartState();
    }

    public void UpdateGameState()
    {
        if (currentState != null)
            currentState.UpdateState();
    }
}
