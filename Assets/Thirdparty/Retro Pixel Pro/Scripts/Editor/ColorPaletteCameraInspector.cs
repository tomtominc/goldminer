﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using UnityEditor.SceneManagement;

namespace Framework
{
    [CustomEditor(typeof(ColorPaletteCamera))]
    public class ColorPaletteCameraInspector : Editor
    {
        private ColorPaletteCamera instance;

        private void OnEnable()
        {
            instance = (ColorPaletteCamera)target;
        }

        public override void OnInspectorGUI()
        {
            instance.horizontalResolution = EditorGUILayout.IntField("Horizontal Resolution",
                instance.horizontalResolution);

            instance.verticalResolution = EditorGUILayout.IntField("Horizontal Resolution",
                instance.verticalResolution);

            if (GUILayout.Button("Match Screen Resolution"))
            {
                instance.horizontalResolution = Screen.width;
                instance.verticalResolution = Screen.height;
            }

            instance.strength = EditorGUILayout.Slider("Strength",
                instance.strength, 0f, 1f);

            if (GUILayout.Button("Load All Colormaps"))
            {
                Undo.RecordObject(instance, "Loaded colormaps");

                instance.currentColormap = 0;
                instance.colormaps = new List < Colormap >();

                foreach (string guid in AssetDatabase.FindAssets("t:Colormap"))
                {
                    string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                    Colormap colormap = AssetDatabase.LoadAssetAtPath(assetPath, typeof(Colormap)) as Colormap;

                    instance.colormaps.Add(colormap);
                }

                if (instance.colormaps.Count <= 0)
                    instance.currentColormap = -1;
            }

            if (instance.colormaps != null && instance.colormaps.Count > 0)
            {
                int currentColormap = EditorPrefs.GetInt("Current Color Map", 0);
               
                instance.currentColormap = EditorGUILayout.Popup("Color Palette", instance.currentColormap, instance.colormaps.Select(x => x.name).ToArray());

                if (instance.currentColormap != currentColormap)
                {
                    
                    Undo.RecordObject(instance, "Changed Colormap");
                    instance.colormap = instance.colormaps[instance.currentColormap];
                    instance.colormap.changedInternally = true;
                    EditorUtility.SetDirty(instance);
                }

                EditorPrefs.SetInt("Current Color Map", instance.currentColormap);
            }
        }
    }

}
