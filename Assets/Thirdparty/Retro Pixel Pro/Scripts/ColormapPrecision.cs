﻿using UnityEngine;
using System.Collections;

namespace Framework
{

	[System.Serializable]
	public enum ColormapPrecision
	{
		Low,
		Medium,
		High,
		Overkill,
		StupidOverkill
	}

}
